FROM ubuntu

RUN apt update && \
    apt install mosquitto \
        openssl -y && \
    mkdir -p /mosquitto/config /mosquitto/data /mosquitto/log /mosquitto/passwd && \
    chown -R mosquitto:mosquitto /mosquitto

COPY ./config/mosquitto.conf /mosquitto/config
COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
