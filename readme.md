# Imagem Docker com MQTT - Eclipse Paho
Ubuntu image with MQTT Auth and SSL

## Config SSL
```
openssl genrsa -des3 -out ca.key 2048
openssl req -new -x509 -days 1826 -key ca.key -out ca.crt
openssl genrsa -out server.key 2048
openssl req -new -out server.csr -key server.key
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 360
```

# Run Docker
docker run -p 8883:8883 -p 9001:9001 -v (pwd)/certs:/etc/mosquitto/certs vitor/mqtt

# IP - Domain Configured
172.17.0.2

#Test
mosquitto_pub -h 172.17.0.2 -p 8883 -m "VITOR" -t teste -u app -P vitor123 --cafile ./certs/server.crt 
mosquitto_sub -h 172.17.0.2 -p 8883 -t teste -u app -P vitor123 --cafile ./certs/server.crt